# Adoption Option

Adoption options is a webapp that allows users to signup and post animals that they are wanting to put up for adoption.
Users passwords are hashed on signup and login. Users are also authenticated using a local passport strategy. This project was done jusing Vue, Node, MongoDB, HTML and CSS

![Adoption](images/adoption.png)
