const mongoose = require('mongoose');
const bcrypt = require('bcrypt');

//change db to correct one!!
mongoose.connect('mongodb+srv://eddy:password1234@cluster0-odlqq.mongodb.net/midterm?retryWrites=true&w=majority', { 
    useNewUrlParser: true, 
    useUnifiedTopology: true
});

var Animal = mongoose.model('Animal', { 
    animal: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    breed: { 
        type: String,
        required: true
    },
    age:  {
        type: Number,
        required: true
    },
    gender:  { 
        type: String,
        required: true
    },
    color:  { 
        type: String,
        required: true
    },
    description:  {
        type: String,
        required: true
    },
    scheduled:  {
        type: Boolean,
        required: true
    }
});

var userSchema = new mongoose.Schema({
    email: {
        type: String,
        required: true,
        unique: true
    },
    encryptedPassword: {
        type: String,
        required: true
    },
    fName: {
        type: String,
        required: true
    },
    lName: {
        type: String,
        required: true
    }

});

userSchema.methods.setEncryptedPassword = function ( plainPassword, callbackFunction) {
    //this is a user instance
    bcrypt.hash(plainPassword, 12).then(hash => {
        //still the user instance because of the arrow
        this.encryptedPassword = hash;
        callbackFunction();
    });
};

userSchema.methods.verifyPassword = function ( plainPassword, callbackFunction ) {
    bcrypt.compare(plainPassword, this.encryptedPassword).then(result => {
        callbackFunction(result);
    });
};

var User = mongoose.model('User', userSchema);

module.exports = {
    Animal: Animal,
    User: User
};